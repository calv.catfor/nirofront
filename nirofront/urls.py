from django.contrib import admin
from django.urls import path, include
from django.conf.urls import handler404, url
from django.contrib.auth.decorators import login_required, permission_required
from .views import *

urlpatterns = [
    path('accounts/', include('django.contrib.auth.urls')),
    path('', login_required(Login.as_view())),
    path('home/', login_required(Home.as_view()), name='home'),
    path('product/', login_required(Product.as_view()), name='products'),
    path('order/', login_required(Order.as_view()), name='orders'),
    path('user/', login_required(User.as_view()), name='users'),
    path('report/', login_required(Report.as_view()), name='reports'),
]

handler404 =  Error404View.as_view()