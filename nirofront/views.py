import logging, os, sys, random, requests, json
from django.contrib.auth import authenticate,get_user_model,login,logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.views import View

logging.basicConfig(level=logging.DEBUG, format='[%(levelname)s] > %(message)s')
logger = logging.getLogger(__name__)

headers = {'Content-Type': 'application/json; charset=utf-8'}

def LoggerError(pseudoid = None):
    exc_type, exc_obj, exc_tb = sys.exc_info()
    err = f"{('[{}] - '.format(pseudoid) if pseudoid else '')}LoggerERROR {exc_type}, {exc_tb.tb_frame.f_code.co_filename}, {exc_tb.tb_lineno}"
    logger.error(err)
    return err

def get_id():
    return random.randint(0,9000)

# template de error 404 personalizado
class Error404View(TemplateView):
    template_name = "404.html"

class Product(View):
    
    def get(self, request):
        try:
            pseudoid = get_id()
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            r = requests.get('http://localhost:8000/productos/')
            data = json.loads(r.content)
            # print(data)
            return render(request, "crud_product.html", context = data)
        except:
            LoggerError()
            return render(request, "500.html")
    
    
    def post(self, request):
        try:
            pseudoid = get_id()
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            payload = dict()
            payload["idp"] = request.POST.get("idp")
            payload["name"] = request.POST.get("name")
            payload["category"] = request.POST.get("category")
            payload["count"] = request.POST.get("stock")
            payload["price"] = request.POST.get("price")
            if request.POST.get("delete"):
                payload["delete"] = request.POST.get("delete")
                r = requests.put('http://localhost:8000/productos/', headers=headers, json = payload)
            elif request.POST.get("idp"):
                r = requests.put('http://localhost:8000/productos/', headers=headers, json = payload)
            else:
                r = requests.post('http://localhost:8000/productos/', headers=headers, json = payload)
            print(r.status_code)
            return Product.get(self,request)
            # return render(request, "crud_product.html", context = data)
        except:
            LoggerError()
            return render(request, "500.html")
    
    def put(self, request):
        try:
            pseudoid = get_id()
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            payload = dict()
            payload["idp"] = request.PUT.get("idp")
            payload["name"] = request.PUT.get("name")
            payload["category"] = request.PUT.get("category")
            payload["count"] = request.PUT.get("stock")
            payload["price"] = request.PUT.get("price")
            r = requests.put('http://localhost:8000/productos/', headers=headers, json = payload)
            print(r.status_code)
            return Product.get(self,request)
            # return render(request, "crud_product.html", context = data)
        except:
            LoggerError()
            return render(request, "500.html")

class Order(View):
    
    def get(self, request):
        try:
            pseudoid = get_id()
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            headers = {'pkuser': str(request.user.id),'Content-Type': 'application/json; charset=utf-8'}
            r = requests.get('http://localhost:8000/ordenes/', headers = headers)
            data = dict()
            if r.status_code == 200:
                data = json.loads(r.content)
            print(data)
            return render(request, "crud_order.html", context = data)
        except:
            LoggerError()
            return render(request, "500.html")
    
    
    def post(self, request):
        try:
            pseudoid = get_id()
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            headers = {'pkuser': str(request.user.id),'Content-Type': 'application/json; charset=utf-8'}
            payload = dict()
            payload["client"] = request.POST.get("client")
            payload["service"] = request.POST.get("service")
            payload["product"] = request.POST.get("product")
            payload["amount"] = request.POST.get("amount")
            if request.POST.get("delete"):
                payload["delete"] = request.POST.get("delete")
                r = requests.post('http://localhost:8000/ordenes/', headers=headers, json = payload)
            else:
                r = requests.post('http://localhost:8000/ordenes/', headers=headers, json = payload)
            print(r.status_code)
            return Order.get(self,request)
            # return render(request, "crud_product.html", context = data)
        except:
            LoggerError()
            return render(request, "500.html")

class Report(View):
    
    def get(self, request):
        try:
            pseudoid = get_id()
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            headers = {'pkuser': str(request.user.id),'Content-Type': 'application/json; charset=utf-8'}
            r = requests.get('http://localhost:8000/reporte/', headers = headers)
            data = dict()
            if r.status_code == 200:
                data = json.loads(r.content)
            print(data)
            return render(request, "crud_report.html", context = data)
        except:
            LoggerError()
            return render(request, "500.html")

    def post(self, request):
        try:
            pseudoid = get_id()
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            headers = {'pkuser': str(request.user.id),'Content-Type': 'application/json; charset=utf-8'}
            payload = dict()
            payload["ini"] = request.POST.get("ini")
            payload["end"] = request.POST.get("end")
            r = requests.post('http://localhost:8000/reporte/', headers = headers, json=payload )
            data = dict()
            data["ini"] = request.POST.get("ini")
            data["end"] = request.POST.get("end")
            if r.status_code == 200:
                data = json.loads(r.content)
            print(data)
            return render(request, "crud_report.html", context = data)
        except:
            LoggerError()
            return render(request, "500.html")

class User(View):
    '''
        Se duplica el trabajo por una buena razón, para asegurar que la capa de BD tenga por su cuenta los registros de los usuarios
    '''
    def get(self, request):
        try:
            pseudoid = get_id()
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            r = requests.get('http://localhost:8000/usuarios/')
            data = json.loads(r.content)
            logger.info(data)
            return render(request, "crud_user.html", context = data)
        except:
            LoggerError()
            return render(request, "500.html")
    
    
    def post(self, request):
        try:
            pseudoid = get_id()
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            payload = dict()
            payload["username"] = request.POST.get("name")
            payload["email"] = request.POST.get("email")
            payload["password"] = request.POST.get("password")
            r = requests.post('http://localhost:8000/usuarios/', headers=headers, json = payload)
            # print(r.status_code)
            User.objects.create_user(username=request.POST.get("name"), email=request.POST.get("email"), password=request.POST.get("password"))
            return User.get(self,request)
            # return render(request, "crud_product.html", context = data)
        except:
            LoggerError()
            return render(request, "500.html")
    
    def put(self, request):
        try:
            pseudoid = get_id()
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            payload = dict()
            payload["idp"] = request.PUT.get("idp")
            payload["name"] = request.PUT.get("name")
            payload["category"] = request.PUT.get("category")
            payload["count"] = request.PUT.get("stock")
            payload["price"] = request.PUT.get("price")
            r = requests.put('http://localhost:8000/productos/', headers=headers, json = payload)
            return Product.get(self,request)
            # return render(request, "crud_product.html", context = data)
        except:
            LoggerError()
            return render(request, "500.html")

class Home(View):
    def get(self, request):
        try:
            pseudoid = get_id()
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            return render(request, "home.html")
        except:
            return render(request, "500.html")

class Login(View):
    def get(self, request):
        try:
            pseudoid = get_id()
            logger.info(f"[ {pseudoid} | {self.__class__.__qualname__} > {request.method}]")
            return render(request, "login.html")
        except:
            return render(request, "500.html")
        
    def post(self, request):
        logger.info("Cargando pagina de login")
        next = request.GET.get('next')
        print(request.POST)
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if not user:
            login(request,user)
        if next:
            return redirect(next)
        return render(request, "home.html")
        # return HttpResponse('result')